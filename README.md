# EShopLite

A simple version of an online e-commerce marketplace. A vistor register, login, shop and view purchase history, etc.

## Demo

[EShopLite](https://e-shop-lite.web.app/)

## Spring Boot Backend Repository

-   [Link](https://gitlab.com/tanjiawei/e-shop-lite-spring-boot)

## Built With

-   [Angular](https://angular.io/) - Frontend
-   [BootStrap](https://angular.io/) - Styling and components on the frontend
-   [Okta](https://developer.okta.com/) - Application security and user management
-   [Ng-BootStrap](https://angular.io/) - Styling and components on the frontend
-   [Angular Material](https://material.angular.io/) - Styling and components on the frontend
-   [Spring Boot](https://spring.io/) - Backend
-   [Hibernate](http://hibernate.org/) - Backend Domain model persistence for relational databases
-   [Maven](https://maven.apache.org/) - Dependency Management
-   Others, etc

## Note

-   This demo app relies on free hosting which operates slow and has a query limit (and error prone sometimes).

## Author

[Westley Tan](https://westleytan.com/)
