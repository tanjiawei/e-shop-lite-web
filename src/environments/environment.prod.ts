export const environment = {
    production: true,
    apiUrl: 'https://e-shop-lite-spring.herokuapp.com/api',
    tempUrl: 'https://elites3-image-url.herokuapp.com/',
    oidc: {
        clientId: '0oanr968t29ouGzsP5d6',
        issuer: 'https://dev-21223594.okta.com/oauth2/default',
        redirectUri: 'https://e-shop-lite.web.app/login/callback',
        scopes: ['openid', 'profile', 'email'],
    },
};
