import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'unit',
})
export class UnitPipe implements PipeTransform {
    transform(value: string, arg?: string): string {
        if (!value) return null;

        value += arg ? `/${arg}` : '/EA';

        return value;
    }
}
