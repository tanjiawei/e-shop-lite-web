import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'tax',
})
export class TaxPipe implements PipeTransform {
    transform(value: number, gross?: boolean, taxRate?: number): number {
        if (!value) return null;

        value *= taxRate ? (!gross ? 0 : 1) + taxRate : (!gross ? 0 : 1) + 0.13;

        return value;
    }
}
