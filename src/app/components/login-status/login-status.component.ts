import { Component, OnInit } from '@angular/core';
import { OktaAuthService, UserClaims } from '@okta/okta-angular';
import { StorageUtilService } from 'src/app/services/storage-util.service';

@Component({
    selector: 'login-status',
    templateUrl: './login-status.component.html',
    styleUrls: ['./login-status.component.css'],
})
export class LoginStatusComponent implements OnInit {
    isAuthenticated = false;
    oktaUser: UserClaims = null;

    constructor(private oktaAuthService: OktaAuthService) {}

    ngOnInit(): void {
        this.oktaAuthService.$authenticationState.subscribe((state) => {
            this.isAuthenticated = state;
            this.getUserDetails();
        });
    }

    getUserDetails() {
        if (!this.isAuthenticated) return;

        this.oktaAuthService.getUser().then((user) => {
            this.oktaUser = user;

            StorageUtilService.addUserEmail(this.oktaUser.email);
            StorageUtilService.addUserFirstName(this.oktaUser.given_name);
            StorageUtilService.addUserLastName(this.oktaUser.family_name);
        });
    }

    logout() {
        this.oktaAuthService.logout();
    }
}
