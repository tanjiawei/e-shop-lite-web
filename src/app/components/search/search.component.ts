import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'product-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.css'],
})
export class SearchComponent implements OnInit {
    search: string;
    constructor(private router: Router) {}

    ngOnInit(): void {}

    handleSearch() {
        if (!this.search?.trim()) return;
        this.router.navigateByUrl(`/search/${this.search}`);
        this.search = null;
    }
}
