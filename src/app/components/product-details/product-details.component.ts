import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable, of } from 'rxjs';
import { CartItem } from 'src/app/common/cart-item';
import { Product } from 'src/app/common/product';
import { CartService } from 'src/app/services/cart.service';
import { ProductService } from 'src/app/services/product.service';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-product-details',
    templateUrl: './product-details.component.html',
    styleUrls: ['./product-details.component.css'],
})
export class ProductDetailsComponent implements OnInit {
    product$: Observable<Product>;
    hrk = environment.tempUrl;
    constructor(
        private productService: ProductService,
        private route: ActivatedRoute,
        private cartService: CartService
    ) {}

    ngOnInit(): void {
        this.route.paramMap.subscribe(() => this.handleProductDetails());
    }

    handleProductDetails(): void {
        const productId = +this.route.snapshot.paramMap.get('id');
        this.product$ = this.productService.getProductByIdWithPagination(
            productId
        );
    }

    addToCart(product: Product): void {
        this.cartService.addToCart(new CartItem(product));
    }
}
