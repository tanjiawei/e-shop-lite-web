import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { CartService } from 'src/app/services/cart.service';

@Component({
    selector: 'cart-status',
    templateUrl: './cart-status.component.html',
    styleUrls: ['./cart-status.component.css'],
})
export class CartStatusComponent implements OnInit {
    cartStat$;

    constructor(private cartService: CartService) {}

    ngOnInit(): void {
        this.getCartStatus();
    }

    private getCartStatus() {
        this.cartStat$ = this.cartService.cartStat$;
    }
}
