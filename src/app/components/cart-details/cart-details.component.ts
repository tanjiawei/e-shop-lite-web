import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { CartItem } from 'src/app/common/cart-item';
import { CartService, TCartStat } from 'src/app/services/cart.service';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-cart-details',
    templateUrl: './cart-details.component.html',
    styleUrls: ['./cart-details.component.css'],
})
export class CartDetailsComponent implements OnInit {
    cartStat$: Observable<TCartStat>;
    hrk = environment.tempUrl;
    constructor(private cartService: CartService) {}

    ngOnInit(): void {
        this.listCartDetails();
    }

    listCartDetails(): void {
        this.cartStat$ = this.cartService.cartStat$;
    }

    incrementQuantity(cartItem: CartItem): void {
        this.cartService.addToCart(cartItem);
    }

    decrementQuantity(cartItem: CartItem): void {
        this.cartService.decrementQuantity(cartItem);
    }

    removeCartItem(cartItem: CartItem) {
        this.cartService.removeCartItem(cartItem);
    }
}
