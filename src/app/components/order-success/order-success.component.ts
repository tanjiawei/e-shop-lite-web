import { Component, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { StorageUtilService } from 'src/app/services/storage-util.service';

@Component({
    selector: 'app-order-success',
    templateUrl: './order-success.component.html',
    styleUrls: ['./order-success.component.css'],
})
export class OrderSuccessComponent implements OnDestroy {
    orderTracking: string;

    constructor(private router: Router) {
        const tracking = StorageUtilService.getOrderTrackingId();

        if (tracking) {
            this.orderTracking = tracking;
        } else this.router.navigateByUrl('/products');
    }

    ngOnDestroy(): void {
        StorageUtilService.removeOrderTrackingId();
    }
}
