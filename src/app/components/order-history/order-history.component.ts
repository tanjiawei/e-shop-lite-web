import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { OrderHistory } from 'src/app/common/order-history';
import {
    GetOrderHistoryResponse,
    OrderHistoryService,
} from 'src/app/services/order-history.service';
import { StorageUtilService } from 'src/app/services/storage-util.service';

@Component({
    selector: 'app-order-history',
    templateUrl: './order-history.component.html',
    styleUrls: ['./order-history.component.css'],
})
export class OrderHistoryComponent implements OnInit {
    orderHistory$: Observable<OrderHistory[]>;

    constructor(private orderHistoryService: OrderHistoryService) {}

    ngOnInit(): void {
        this.getOrderHistory();
    }

    getOrderHistory(): void {
        const userEmail = StorageUtilService.getUserEmail();
        this.orderHistory$ = this.orderHistoryService
            .getOrderHistoryByEmailWithPagination(userEmail)
            .pipe(map((res: GetOrderHistoryResponse) => res._embedded.orders));
    }
}
