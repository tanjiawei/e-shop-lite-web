import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { CartItem } from 'src/app/common/cart-item';
import { Product } from 'src/app/common/product';
import { CartService } from 'src/app/services/cart.service';
import {
    GetProductsResponse,
    ProductService,
} from 'src/app/services/product.service';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'product-list',
    templateUrl: './product-list.component.html',
    styleUrls: ['./product-list.component.css'],
})
export class ProductListComponent implements OnInit {
    products$: Observable<Product[]>;
    currentCategoryId: number;
    previousCategoryId: number;
    currentCategoryName: string;
    searchMode = false;
    hrk = environment.tempUrl;
    // pagination
    pageNumber = 1;
    pageSize = 8;
    totalElements = 0;

    keyWord: string;
    previousKeyword: string;

    constructor(
        private productService: ProductService,
        private route: ActivatedRoute,
        private cartService: CartService
    ) {}

    ngOnInit(): void {
        this.route.paramMap.subscribe(() => this.listProduct());
    }

    listProduct() {
        this.currentCategoryName =
            this.route.snapshot.paramMap.get('name') ?? 'All';

        this.keyWord = this.route.snapshot.paramMap.get('keyword');

        this.currentCategoryId = +this.route.snapshot.paramMap.get('id');

        if (this.keyWord) return this.handleListProductsWithSearch();

        if (this.currentCategoryId)
            return this.handleListProductsWithCategory();

        this.handleListProducts();
    }

    updatePageSize(event) {
        this.pageSize = parseInt(event.currentTarget.value);
        this.pageNumber = 1;
        this.listProduct();
    }

    handleListProductsWithSearch() {
        this.handleResetPageOnKeyWordChange();

        this.products$ = this.productService
            .getProductsBySearchNameWithPagination(
                this.pageNumber - 1,
                this.pageSize,
                this.keyWord
            )
            .pipe(map(this.handleProcessProductsResponse.bind(this)));
    }

    handleListProductsWithCategory() {
        this.handleResetPageOnCategoryIdChange();

        this.products$ = this.productService
            .getProductsByCategoryWithPagination(
                this.pageNumber - 1,
                this.pageSize,
                this.currentCategoryId
            )
            .pipe(map(this.handleProcessProductsResponse.bind(this)));
    }

    handleListProducts() {
        this.products$ = this.productService
            .getAllProductsWithPagination(this.pageNumber - 1, this.pageSize)
            .pipe(map(this.handleProcessProductsResponse.bind(this)));
    }

    addToCart(product: Product): void {
        this.cartService.addToCart(new CartItem(product));
    }

    private handleResetPageOnCategoryIdChange() {
        this.previousCategoryId !== this.currentCategoryId &&
            (this.pageNumber = 1);

        this.previousCategoryId = this.currentCategoryId;
    }

    private handleResetPageOnKeyWordChange() {
        this.previousKeyword !== this.keyWord && (this.pageNumber = 1);

        this.previousKeyword = this.keyWord;
    }

    private handleProcessProductsResponse({
        _embedded,
        page,
    }: GetProductsResponse): Product[] {
        this.pageNumber = page.number + 1;
        this.totalElements = page.totalElements;
        return _embedded.products;
    }
}
