import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ProductCategory } from 'src/app/common/product-category';
import { ProductService } from 'src/app/services/product.service';

@Component({
    selector: 'product-category-manu',
    templateUrl: './product-category-manu.component.html',
    styleUrls: ['./product-category-manu.component.css'],
})
export class ProductCategoryManuComponent implements OnInit {
    productCategories$: Observable<ProductCategory[]>;

    constructor(private productService: ProductService) {}

    ngOnInit(): void {
        this.listProductCategories();
    }

    listProductCategories() {
        this.productCategories$ = this.productService.getProductCategories();
    }
}
