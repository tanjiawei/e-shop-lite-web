import { Component, OnInit } from '@angular/core';
import { OktaAuthService } from '@okta/okta-angular';
import * as OktaSignIn from '@okta/okta-signin-widget';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
    oktaSignin;

    constructor(private oktaAuthService: OktaAuthService) {
        this.oktaSignin = new OktaSignIn({
            logo: '../../../asset/images/logo.png',
            features: {
                registration: true,
            },
            baseUrl: environment.oidc.issuer.split('/oauth2')[0],
            clientId: environment.oidc.clientId,
            redirectUri: environment.oidc.redirectUri,
            authParams: {
                pkce: true,
                issuer: environment.oidc.issuer,
                scopes: environment.oidc.scopes,
            },
        });
    }

    ngOnInit(): void {
        this.oktaSignin.remove();

        this.oktaSignin.renderEl(
            {
                el: '#okta-sign-in-widget',
            },
            (res) => {
                if (res.status === 'SUCCESS')
                    this.oktaAuthService.loginRedirect('/');
            },
            (err) => {
                throw err;
            }
        );
    }
}
