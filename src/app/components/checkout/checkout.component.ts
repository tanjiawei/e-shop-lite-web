import { Component, OnInit } from '@angular/core';
import {
    AbstractControl,
    FormBuilder,
    FormGroup,
    ValidatorFn,
    Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Address } from 'src/app/common/address';
import { CartItem } from 'src/app/common/cart-item';
import { Country } from 'src/app/common/country';
import { Order } from 'src/app/common/order';
import { OrderItem } from 'src/app/common/order-item';
import { Purchase } from 'src/app/common/purchase';
import { State } from 'src/app/common/state';
import { CartService, TCartStat } from 'src/app/services/cart.service';
import { CheckoutService } from 'src/app/services/checkout.service';
import { FormUtilService } from 'src/app/services/form-util.service';
import { LocationService } from 'src/app/services/location.service';
import { StorageUtilService } from 'src/app/services/storage-util.service';
import { FormValidators } from 'src/app/validators/form-validators';

@Component({
    selector: 'app-checkout',
    templateUrl: './checkout.component.html',
    styleUrls: ['./checkout.component.css'],
})
export class CheckoutComponent implements OnInit {
    cartStat$: Observable<TCartStat>;
    creditCardMonths$: Observable<number[]>;
    creditCardYears$: Observable<number[]>;
    countries$: Observable<Country[]>;
    statesByCountryOnShipping$: Observable<State[]>;
    statesByCountryOnBilling$: Observable<State[]>;

    cartStat: TCartStat;
    checkoutFormGroup: FormGroup;

    formStringValidatorFns(minLength: number): ValidatorFn[] {
        return [
            Validators.required,
            Validators.minLength(minLength),
            FormValidators.notOnlyWhiteSpace,
        ];
    }

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private cartService: CartService,
        private formUtilService: FormUtilService,
        private locationService: LocationService,
        private checkoutService: CheckoutService
    ) {}

    ngOnInit(): void {
        this.reviewCartDetails();

        this.creditCardMonths$ = this.formUtilService.getCreditCardMonths();
        this.creditCardYears$ = this.formUtilService.getCreditCardYears();
        this.countries$ = this.locationService.getCountries();

        this.checkoutFormGroup = this.fb.group({
            customer: this.fb.group({
                firstName: [
                    StorageUtilService.getUserFirstName(),
                    this.formStringValidatorFns(2),
                ],
                lastName: [
                    StorageUtilService.getUserLastName(),
                    this.formStringValidatorFns(2),
                ],
                email: [
                    StorageUtilService.getUserEmail(),
                    [
                        Validators.required,
                        Validators.pattern(
                            '^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'
                        ),
                    ],
                ],
            }),
            shippingAddress: this.fb.group({
                street: ['', this.formStringValidatorFns(5)],
                city: ['', this.formStringValidatorFns(3)],
                state: ['', Validators.required],
                country: ['', Validators.required],
                postalCode: ['', this.formStringValidatorFns(4)],
            }),
            billingAddress: this.fb.group({
                street: ['', this.formStringValidatorFns(5)],
                city: ['', this.formStringValidatorFns(3)],
                state: ['', Validators.required],
                country: ['', Validators.required],
                postalCode: ['', this.formStringValidatorFns(4)],
            }),
            creditCard: this.fb.group({
                cardType: ['', Validators.required],
                nameOnCard: ['', this.formStringValidatorFns(4)],
                cardNumber: [
                    '',
                    [Validators.required, FormValidators.isValidCreditCard],
                ],
                securityCode: [
                    '',
                    [Validators.required, Validators.pattern('[0-9]{3}')],
                ],
                expirationMonth: ['', Validators.required],
                expirationYear: ['', Validators.required],
            }),
        });
    }

    reviewCartDetails() {
        this.cartStat$ = this.cartService.cartStat$.pipe(
            tap((stat) => (this.cartStat = stat))
        );
    }

    onSubmit() {
        if (this.checkoutFormGroup.invalid)
            return this.checkoutFormGroup.markAllAsTouched();

        const [cartItems, totalQuantity, totalPrice] = this.cartStat;

        const customer = this.checkoutFormGroup.get('customer').value;
        const shippingAddress: Address = this.formUtilService.mapFormToAddress(
            this.checkoutFormGroup.get('shippingAddress').value
        );
        const billingAddress: Address = this.formUtilService.mapFormToAddress(
            this.checkoutFormGroup.get('billingAddress').value
        );
        const order = new Order(totalPrice, totalQuantity);
        const orderItems = cartItems.map(
            (cartItem: CartItem) => new OrderItem(cartItem)
        );

        const purchase = new Purchase(
            customer,
            shippingAddress,
            billingAddress,
            order,
            orderItems
        );

        this.checkoutService.placeOrder(purchase).subscribe({
            next: (res) => {
                this.reset();
                StorageUtilService.addOrderTrackingId(res.orderTrackingNumber);

                this.router.navigate([
                    '/order-success',
                    res.orderTrackingNumber,
                ]);
            },
            error: (err) => alert(err.message),
        });
    }

    copyShippingAddressToBillingAddress($event) {
        if ($event.target.checked) {
            this.checkoutFormGroup.controls.billingAddress.setValue(
                this.checkoutFormGroup.controls.shippingAddress.value
            );
            this.statesByCountryOnBilling$ = this.statesByCountryOnShipping$;
        } else {
            this.checkoutFormGroup.controls.billingAddress.reset();
            this.statesByCountryOnBilling$ = of([]);
        }
    }

    getStates(formGroupName: string) {
        const fromGroup = this.checkoutFormGroup.get(formGroupName);
        const countryId = fromGroup.value.country.id;

        if (formGroupName === 'shippingAddress')
            this.statesByCountryOnShipping$ = this.locationService.getStatesByCountryId(
                countryId
            );
        else
            this.statesByCountryOnBilling$ = this.locationService.getStatesByCountryId(
                countryId
            );
    }

    handleExpirationDateReconciliation() {
        const creditCard = this.checkoutFormGroup.get('creditCard');
        const selectedYear = Number(creditCard.value.expirationYear);

        if (selectedYear > new Date().getFullYear())
            this.formUtilService.handleMonthsReconciliation(1);
        else
            this.formUtilService.handleMonthsReconciliation(
                new Date().getMonth() + 2
            );
    }

    private reset(): void {
        this.cartService.resetCart();
        this.checkoutFormGroup.reset();
    }

    // customer
    get firstName(): AbstractControl {
        return this.checkoutFormGroup.get('customer.firstName');
    }
    get lastName(): AbstractControl {
        return this.checkoutFormGroup.get('customer.lastName');
    }
    get email(): AbstractControl {
        return this.checkoutFormGroup.get('customer.email');
    }
    // shippingAddress
    get street_shipping(): AbstractControl {
        return this.checkoutFormGroup.get('shippingAddress.street');
    }
    get city_shipping(): AbstractControl {
        return this.checkoutFormGroup.get('shippingAddress.city');
    }
    get state_shipping(): AbstractControl {
        return this.checkoutFormGroup.get('shippingAddress.state');
    }
    get country_shipping(): AbstractControl {
        return this.checkoutFormGroup.get('shippingAddress.country');
    }
    get postalCode_shipping(): AbstractControl {
        return this.checkoutFormGroup.get('shippingAddress.postalCode');
    }
    // billingAddress;
    get street_billing(): AbstractControl {
        return this.checkoutFormGroup.get('billingAddress.street');
    }
    get city_billing(): AbstractControl {
        return this.checkoutFormGroup.get('billingAddress.city');
    }
    get state_billing(): AbstractControl {
        return this.checkoutFormGroup.get('billingAddress.state');
    }
    get country_billing(): AbstractControl {
        return this.checkoutFormGroup.get('billingAddress.country');
    }
    get postalCode_billing(): AbstractControl {
        return this.checkoutFormGroup.get('billingAddress.postalCode');
    }

    // creditCard
    get cardType() {
        return this.checkoutFormGroup.get('creditCard.cardType');
    }
    get nameOnCard() {
        return this.checkoutFormGroup.get('creditCard.nameOnCard');
    }
    get cardNumber() {
        return this.checkoutFormGroup.get('creditCard.cardNumber');
    }
    get securityCode() {
        return this.checkoutFormGroup.get('creditCard.securityCode');
    }
    get expirationMonth() {
        return this.checkoutFormGroup.get('creditCard.expirationMonth');
    }
    get expirationYear() {
        return this.checkoutFormGroup.get('creditCard.expirationYear');
    }
}
