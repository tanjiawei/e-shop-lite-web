import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {
    OKTA_CONFIG,
    OktaAuthGuard,
    OktaAuthModule,
    OktaCallbackComponent,
} from '@okta/okta-angular';

import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
import { CartDetailsComponent } from './components/cart-details/cart-details.component';
import { CartStatusComponent } from './components/cart-status/cart-status.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { LoginStatusComponent } from './components/login-status/login-status.component';
import { LoginComponent } from './components/login/login.component';
import { MembersPageComponent } from './components/members-page/members-page.component';
import { OrderHistoryComponent } from './components/order-history/order-history.component';
import { OrderSuccessComponent } from './components/order-success/order-success.component';
import { ProductCategoryManuComponent } from './components/product-category-manu/product-category-manu.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { ProductListComponent } from './components/product-list/product-list.component';
import { SearchComponent } from './components/search/search.component';
import { TaxPipe } from './pipes/tax.pipe';
import { UnitPipe } from './pipes/unit.pipe';
import { AuthInterceptorService } from './services/auth-interceptor.service';
import { ProductService } from './services/product.service';

const oktaConfig = Object.assign(
    {
        onAuthRequired: (_oktaAuth, injecter) =>
            injecter.get(Router).navigate(['/login']),
    },
    environment.oidc
);
console.log('redir', environment.oidc.redirectUri);
@NgModule({
    declarations: [
        AppComponent,
        ProductListComponent,
        ProductCategoryManuComponent,
        SearchComponent,
        ProductDetailsComponent,
        CartStatusComponent,
        CartDetailsComponent,
        UnitPipe,
        TaxPipe,
        CheckoutComponent,
        OrderSuccessComponent,
        LoginComponent,
        LoginStatusComponent,
        MembersPageComponent,
        OrderHistoryComponent,
    ],
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        FormsModule,
        NgbModule,
        HttpClientModule,
        BrowserAnimationsModule,
        MatTableModule,
        OktaAuthModule,
        RouterModule.forRoot([
            { path: 'login/callback', component: OktaCallbackComponent },
            { path: 'login', component: LoginComponent },
            {
                path: 'profile/order-history',
                component: OrderHistoryComponent,
                canActivate: [OktaAuthGuard],
            },
            {
                path: 'profile',
                component: MembersPageComponent,
                canActivate: [OktaAuthGuard],
            },
            { path: 'category/:id/:name', component: ProductListComponent },
            { path: 'category', component: ProductListComponent },
            { path: 'products/:id', component: ProductDetailsComponent },
            { path: 'products', component: ProductListComponent },
            { path: 'order-details', component: ProductListComponent },
            { path: 'search/:keyword', component: ProductListComponent },
            { path: 'cart-details', component: CartDetailsComponent },
            {
                path: 'order-success/:tracking',
                component: OrderSuccessComponent,
            },
            {
                path: 'checkout',
                component: CheckoutComponent,
                canActivate: [OktaAuthGuard],
            },
            { path: '', redirectTo: '/products', pathMatch: 'full' },
            { path: '**', redirectTo: '/products', pathMatch: 'full' },
        ]),
    ],
    providers: [
        ProductService,
        { provide: OKTA_CONFIG, useValue: oktaConfig },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptorService,
            multi: true,
        },
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}
