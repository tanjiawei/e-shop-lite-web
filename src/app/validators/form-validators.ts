import { FormControl, ValidationErrors } from '@angular/forms';

export class FormValidators {
    static notOnlyWhiteSpace(control: FormControl): ValidationErrors {
        return !control.value?.trim()
            ? {
                  notOnlyWhiteSpace: true,
              }
            : null;
    }

    static isValidCreditCard(control: FormControl): ValidationErrors {
        let value = control.value?.trim() || '';

        let sum = 0;
        let shouldDouble = false;
        // loop through values starting at the rightmost side
        for (let i = value.length - 1; i >= 0; i--) {
            let digit = parseInt(value.charAt(i));

            if (shouldDouble) {
                if ((digit *= 2) > 9) digit -= 9;
            }

            sum += digit;
            shouldDouble = !shouldDouble;
        }

        return sum % 10 !== 0 || value.length % 16 !== 0
            ? {
                  isValidCreditCard: {
                      notValid: true,
                  },
              }
            : null;
    }
}
