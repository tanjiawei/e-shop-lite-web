import { Injectable } from '@angular/core';

import { CartItem } from '../common/cart-item';

@Injectable({
    providedIn: 'root',
})
export class StorageUtilService {
    constructor() {}

    // add items
    static addUserEmail(data: string): void {
        sessionStorage.setItem('userEmail', JSON.stringify(data));
    }

    static addUserFirstName(data: string): void {
        sessionStorage.setItem('userFirstName', JSON.stringify(data));
    }

    static addUserLastName(data: string): void {
        sessionStorage.setItem('userLastName', JSON.stringify(data));
    }

    static addCartItems(data: CartItem[]): void {
        sessionStorage.setItem('cart-items', JSON.stringify(data));
    }

    static addOrderTrackingId(data: string): void {
        sessionStorage.setItem('order-tracking-id', JSON.stringify(data));
    }

    // get items
    static getUserEmail(): string {
        return JSON.parse(sessionStorage.getItem('userEmail'));
    }

    static getUserFirstName(): string {
        return JSON.parse(sessionStorage.getItem('userFirstName'));
    }

    static getUserLastName(): string {
        return JSON.parse(sessionStorage.getItem('userLastName'));
    }

    static getCartItems(): CartItem[] {
        return JSON.parse(sessionStorage.getItem('cart-items'));
    }

    static getOrderTrackingId(): string {
        return JSON.parse(sessionStorage.getItem('order-tracking-id'));
    }

    // remove items
    static removeCartItems(): void {
        sessionStorage.removeItem('cart-items');
    }

    static removeOrderTrackingId(): void {
        sessionStorage.removeItem('order-tracking-id');
    }
}
