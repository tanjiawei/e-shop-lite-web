import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { OrderHistory } from '../common/order-history';

@Injectable({
    providedIn: 'root',
})
export class OrderHistoryService {
    private baseUrl = `${environment.apiUrl}/orders/search/findByCustomerEmailOrderByDateCreatedDesc`;

    constructor(private http: HttpClient) {}

    getOrderHistoryByEmailWithPagination(
        email: String
    ): Observable<GetOrderHistoryResponse> {
        return this.http
            .get<GetOrderHistoryResponse>(`${this.baseUrl}?email=${email}`)
            .pipe(shareReplay());
    }
}

export interface GetOrderHistoryResponse {
    _embedded: { orders: OrderHistory[] };
    page: {
        size: number;
        totalElements: number;
        totalPages: number;
        number: number;
    };
}
