import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { Country } from '../common/country';
import { State } from '../common/state';

@Injectable({
    providedIn: 'root',
})
export class LocationService {
    private baseUrl = `${environment.apiUrl}/countries`;

    constructor(private http: HttpClient) {}

    getCountries(): Observable<Country[]> {
        return this.getLocationsWithPagination<GetCountriesResponse>().pipe(
            map((res) => res._embedded.countries)
        );
    }

    getStatesByCountryId(countryId: number): Observable<State[]> {
        return this.getLocationsWithPagination<GetStatesResponse>(
            `${countryId}/states`
        ).pipe(map((res) => res._embedded.states));
    }

    private getLocationsWithPagination<R>(
        endpoint: string = ''
    ): Observable<R> {
        return this.http
            .get<R>(`${this.baseUrl}/${endpoint}`)
            .pipe(shareReplay());
    }
}

export interface GetCountriesResponse {
    _embedded: { countries: Country[] };
}

export interface GetStatesResponse {
    _embedded: { states: State[] };
}
