import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { Product } from '../common/product';
import { ProductCategory } from '../common/product-category';

@Injectable({
    providedIn: 'root',
})
export class ProductService {
    private baseUrl = environment.apiUrl;

    constructor(private http: HttpClient) {}

    getAllProductsWithPagination(
        pageNumber: number,
        pageSize: number
    ): Observable<GetProductsResponse> {
        return this.getProductsWithPagination(
            `${this.baseUrl}/products/?page=${pageNumber}&size=${pageSize}`
        );
    }

    getProductsByCategoryWithPagination(
        pageNumber: number,
        pageSize: number,
        categoryId: number
    ): Observable<GetProductsResponse> {
        return this.getProductsWithPagination(
            `${this.baseUrl}/products/search/findByCategoryId?id=${categoryId}&page=${pageNumber}&size=${pageSize}`
        );
    }

    getProductsBySearchNameWithPagination(
        pageNumber: number,
        pageSize: number,
        keyWord: string
    ): Observable<GetProductsResponse> {
        return this.getProductsWithPagination(
            `${this.baseUrl}/products/search/findByNameContaining?name=${keyWord}&page=${pageNumber}&size=${pageSize}`
        );
    }

    private getProductsWithPagination(
        url: string
    ): Observable<GetProductsResponse> {
        return this.http.get<GetProductsResponse>(url).pipe(shareReplay());
    }

    getProductByIdWithPagination(productId: number): Observable<Product> {
        return this.http
            .get<Product>(`${this.baseUrl}/products/${productId}`)
            .pipe(shareReplay());
    }

    getProductCategories(): Observable<ProductCategory[]> {
        return this.http
            .get<GetProductCategoryResponse>(`${this.baseUrl}/product-category`)
            .pipe(
                map((response) => response._embedded.productCategory),
                shareReplay()
            );
    }
}

export interface GetProductsResponse {
    _embedded: { products: Product[] };
    page: {
        size: number;
        totalElements: number;
        totalPages: number;
        number: number;
    };
}
export interface GetProductCategoryResponse {
    _embedded: { productCategory: ProductCategory[] };
}
