import {
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { OktaAuthService } from '@okta/okta-angular';
import { from, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root',
})
export class AuthInterceptorService implements HttpInterceptor {
    constructor(private oktaAuthService: OktaAuthService) {}
    intercept(
        req: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        return from(this.handleAccess(req, next));
    }

    private async handleAccess(
        req: HttpRequest<any>,
        next: HttpHandler
    ): Promise<HttpEvent<any>> {
        const securedEndPoints = [`${environment.apiUrl}/orders`];

        if (!securedEndPoints.some((url) => req.urlWithParams.includes(url)))
            return next.handle(req).toPromise();

        const authReq = req.clone({
            setHeaders: {
                Authorization:
                    'Bearer ' + (await this.oktaAuthService.getAccessToken()),
            },
        });
        return next.handle(authReq).toPromise();
    }
}
