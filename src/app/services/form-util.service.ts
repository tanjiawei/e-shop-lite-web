import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { Address } from '../common/address';
import { Country } from '../common/country';
import { State } from '../common/state';

@Injectable({
    providedIn: 'root',
})
export class FormUtilService {
    private remainingMonthsSubject = new BehaviorSubject<number[]>([]);

    constructor() {
        this.handleMonthsReconciliation(new Date().getMonth() + 2);
    }

    handleMonthsReconciliation(startMonth: number) {
        if (startMonth < 1 || startMonth > 13)
            throw new Error(
                '`Start month` of type `number` must be larger than 1 or smaller than 13'
            );
        const data: number[] = [];
        for (let count = startMonth; count < 13; count++) data.push(count);

        this.remainingMonthsSubject.next(data);
    }

    getCreditCardMonths(): Observable<number[]> {
        return this.remainingMonthsSubject.asObservable();
    }

    getCreditCardYears(): Observable<number[]> {
        // show next 10 years
        const data: number[] = Array(10)
            .fill(null)
            .map((_d, i) => new Date().getFullYear() + i);
        return of(data);
    }

    mapFormToAddress(address: FormAddress): Address {
        return {
            street: address.street,
            city: address.city,
            state: address.state.name,
            country: address.country.name,
            postalCode: address.postalCode,
        };
    }
}

export interface FormAddress {
    street: string;
    city: string;
    state: State;
    country: Country;
    postalCode: string;
}
