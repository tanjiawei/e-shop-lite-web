import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Purchase } from '../common/purchase';

@Injectable({
    providedIn: 'root',
})
export class CheckoutService {
    private purchaseUrl = `${environment.apiUrl}/checkout/purchase`;

    constructor(private http: HttpClient) {}

    placeOrder(purchase: Purchase): Observable<PurchaseResponse> {
        return this.http.post<PurchaseResponse>(this.purchaseUrl, purchase);
    }
}

export interface PurchaseResponse {
    orderTrackingNumber: string;
}
