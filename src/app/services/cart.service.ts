import { Injectable } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { CartItem } from '../common/cart-item';
import { StorageUtilService } from './storage-util.service';

export type TCartStat = [CartItem[], number, number];

@Injectable({
    providedIn: 'root',
})
export class CartService {
    private _cartItems: CartItem[] = [];

    private cartItemsSubject = new BehaviorSubject<CartItem[]>([]);
    private totalQuantitySubject = new BehaviorSubject<number>(0);
    private totalPriceSubject = new BehaviorSubject<number>(0);
    cartStat$: Observable<TCartStat> = combineLatest([
        this.cartItemsSubject.asObservable(),
        this.totalQuantitySubject.asObservable(),
        this.totalPriceSubject.asObservable(),
    ]);

    constructor() {
        const data = StorageUtilService.getCartItems();
        if (data) {
            this._cartItems = data;
            this.computeCartTotals();
        }
    }

    persistCartItemsFromStorage(): void {
        StorageUtilService.addCartItems(this._cartItems);
    }

    addToCart(cartItem: CartItem) {
        const index = this._cartItems.findIndex((c) => c.id === cartItem.id);

        if (index < 0) this._cartItems.push(cartItem);
        else this._cartItems[index].quantity++;

        this.computeCartTotals();
    }

    decrementQuantity(cartItem: CartItem) {
        cartItem.quantity--;

        cartItem.quantity === 0 && this.removeCartItem(cartItem);

        this.computeCartTotals();
    }

    removeCartItem(cartItem: CartItem) {
        this._cartItems = this._cartItems.filter((c) => c.id !== cartItem.id);

        this.computeCartTotals();
    }

    resetCart(): void {
        this._cartItems = [];
        StorageUtilService.removeCartItems();
        this.computeCartTotals();
    }

    private computeCartTotals(): void {
        const totalPriceComputed = this._cartItems
            .map((c) => c.unitPrice * c.quantity)
            .reduce((a, b) => a + b, 0);

        const totalQuantityComputed = this._cartItems
            .map((c) => c.quantity)
            .reduce((a, b) => a + b, 0);

        this.cartItemsSubject.next(this._cartItems);
        this.totalQuantitySubject.next(totalQuantityComputed);
        this.totalPriceSubject.next(totalPriceComputed);

        this.persistCartItemsFromStorage();
    }
}
